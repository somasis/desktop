# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part on "NetworkManager.exlib", which is:
#   Copyright 2008, 2009 Saleem Abdulrasool <compnerd@compnerd.org>
#   Copyright 2010 Brett Witherspoon <spoonb@exherbo.org>
#   Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="https://anongit.freedesktop.org/git/${PN}/${PN}.git"
    require scm-git
else
    require gnome.org [ suffix=tar.xz ]
fi

require toolchain-funcs
require systemd-service udev-rules
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]
require test-dbus-daemon
require vala [ vala_dep=true with_opt=true ]
require option-renames [ renames=[ 'policykit polkit' 'gnutls providers:gnutls' 'systemd providers:systemd' 'consolekit providers:consolekit' ] ]
require meson

export_exlib_phases src_configure src_test src_install pkg_preinst

SUMMARY="A system daemon to configure networking"
DESCRIPTION="
NetworkManager is a set of co-operative tools that make networking simple and straightforward.
Whether WiFi, wired, 3G, or Bluetooth, NetworkManager allows you to quickly move from one network to
another; Once a network has been configured and joined once, it can be detected and re-joined
automatically the next time it's available.
"
HOMEPAGE="https://wiki.gnome.org/Projects/${PN}"

UPSTREAM_DOCUMENTATION="
    ${HOMEPAGE}/users/ [[ lang = en description = [ Users guide ] ]]
    ${HOMEPAGE}/admins/ [[ lang = en description = [ Admin guide ] ]]
    ${HOMEPARE}/developers/ [[ lang = en description = [ Developers guide ] ]]
"

LICENCES="( GPL-2 LGPL-2 )"
SLOT="0"
MYOPTIONS="
    bluetooth [[ description = [ Support for the bluetooth Dial-up Networking Profile (DUN) ] ]]
    doc [[ requires = [ gobject-introspection ] ]]
    journald
    connection-check [[ description = [ Ability to check for internet connectivity ] ]]
    gobject-introspection
    modem-manager [[ description = [ Provides mobile 3G GSM/CDMA support ] ]]
    ofono [[ description = [ Provides mobile 3G GDM/CDMA support (experimental) ] ]]
    polkit
    ppp [[ description = [ Enable PPP/PPPoE support ] ]]
    vapi [[
        description = [ Build Vala bindings ]
        requires = [ gobject-introspection ]
    ]]
    wifi [[ description = [ Enable Wi-Fi support ] ]]
    wifi? (
        ( providers: iwd wpa_supplicant ) [[
            *description = [ Wi-Fi daemon ]
            number-selected = at-least-one
        ]]
    )
    ( providers:
        ( consolekit elogind systemd ) [[
            *description = [ Session tracking provider ]
            number-selected = at-most-one
        ]]
        ( consolekit elogind upower systemd ) [[
            *description = [ Suspend/Resume provider ]
            number-selected = exactly-one
        ]]
        ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
        ( gnutls nss ) [[ number-selected = exactly-one ]]
        ( dhcp dhcpcd internal nettools ) [[
            *description = [ dhcp client provider ]
            number-selected = exactly-one
        ]]
    )
"

DEPENDENCIES+="
    build:
        dev-lang/perl:*
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext[>=0.17]
        sys-kernel/linux-headers[>=3.17]
        virtual/pkg-config
        doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/glib:2[>=2.40]
        net-libs/libndp
        net-libs/libnl:3.0[>=3.2.8]
        sys-apps/dbus[>=1.1]
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        sys-libs/newt[>=0.52.15]
        sys-libs/readline:=
        bluetooth? ( net-wireless/bluez[>=5] )
        connection-check? ( net-misc/curl[>=7.24.0] )
        journald? ( sys-apps/systemd )
        gobject-introspection? (
            gnome-desktop/gobject-introspection:1[>=0.9.6]
            gnome-bindings/pygobject:3
        )
        modem-manager? ( net-wireless/ModemManager[>=0.7.991] )
        ofono? ( net-wireless/ofono )
        polkit? ( sys-auth/polkit:1[>=0.97] )
        ppp? ( net-dialup/ppp )
        providers:consolekit? ( sys-auth/ConsoleKit2 )
        providers:eudev? ( sys-apps/eudev )
        providers:gnutls? ( dev-libs/gnutls[>=2.12] )
        providers:nss? ( dev-libs/nss[>=3.11] )
        providers:systemd? ( sys-apps/systemd[>=209] )
        providers:elogind? ( sys-auth/elogind[>=229] )
        providers:upower? ( sys-apps/upower )
        !app-shells/bash-completion[<2.1-r1] [[
            description = [ NetworkManager now provides its own bash-completions ]
            resolution = upgrade-blocked-before
        ]]
    run:
        app-misc/ca-certificates
        providers:dhcp? ( net-misc/dhcp[>=4] )
        providers:dhcpcd? ( net-misc/dhcpcd[>=6] )
        wifi? (
            providers:iwd? ( net-wireless/iwd )
            providers:wpa_supplicant? ( net-wireless/wpa_supplicant[>=2.6][dbus] )
        )
    suggestion:
        gnome-desktop/network-manager-applet [[
            description = [ A GTK+ GUI for NetworkManager ]
        ]]
        net-apps/NetworkManager-vpnc [[
            description = [ A plugin for Cisco compatible VPN's ]
        ]]
        net-apps/NetworkManager-pptp [[
            description = [ A plugin for Point-to-Point Tunneling Protocol ]
        ]]
        net-dns/dnsmasq [[
            description = [ Required for DNS/DHCP services on exported connections ]
        ]]
        net-firewall/iptables [[
            description = [ Required for exporting connections and creating access points ]
        ]]
        net-misc/iputils [[
            description = [ The arping command is used for APIPA support ]
        ]]
    test:
        gnome-bindings/pygobject:3
"

# TODO(Cogitri): Add proper, multiarch locations for the following binaries:
#  Causes build failures:
#   -Ddnssec_trigger=/usr/$(exhost --target)/libexec/dnssec-trigger-script
#
#  Can't be set without also pulling them in as dependency:
#   -Ddhclient=/usr/$(exhost --target)/bin/dhclient
#   -Ddhcpcd=/usr/$(exhost --target)/bin/dhcpcd

MESON_SRC_CONFIGURE_PARAMS=(
    -Diptables=/usr/$(exhost --target)/bin/iptables
    -Djson_validation=false
    -Dkernel_firmware_dir=/usr/$(exhost --target)/lib/firmware
    -Dlibaudit=no
    -Dlibpsl=false
    -Dnmcli=true
    -Dnmtui=true
    -Dovs=false
    -Dqt=false
    -Dselinux=false
    -Dsession_tracking=no
    -Dsystem_ca_path=/usr/share/ca-certificates
    -Dteamdctl=false
    -Dudev_dir=${UDEVDIR}
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:dhcp -Dconfig_dhcp_default=dhclient'
    'providers:dhcpcd -Dconfig_dhcp_default=dhcpcd'
    'providers:elogind -Dsession_tracking=elogind'
    'providers:gnutls -Dcrypto=gnutls'
    'providers:internal -Dconfig_dhcp_default=internal'
    'providers:nettools -Dconfig_dhcp_default=nettools'
    'providers:nss -Dcrypto=nss'
    'providers:systemd -Dsession_tracking=systemd'
    'providers:systemd -Dsuspend_resume=systemd'
    'providers:elogind -Dsuspend_resume=elogind'
    'providers:upower -Dsuspend_resume=upower'
    'providers:consolekit -Dsuspend_resume=consolekit'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bluetooth bluez5_dun'
    'connection-check concheck'
    'doc docs'
    'gobject-introspection introspection'
    'providers:iwd iwd'
    'journald config_logging_backend_default journal syslog'
    'modem-manager modem_manager'
    ofono
    'polkit polkit'
    'polkit polkit_agent'
    ppp
    'providers:consolekit session_tracking_consolekit'
    'providers:systemd systemd_journal'
    "providers:systemd systemdsystemunitdir ${SYSTEMDSYSTEMUNITDIR} no"
    vapi
    wifi
    'wifi wext'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=yes -Dtests=no'
)

NetworkManager-meson_src_configure() {
    local args=()

    ld-is-lld && args+=( -Dld_gc=false )

    meson_src_configure "${args[@]}"
}

NetworkManager-meson_src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

NetworkManager-meson_src_install() {
    meson_src_install

    install_openrc_files

    keepdir /etc/NetworkManager/{conf.d,dispatcher.d{,/pre-{down,up}.d,/no-wait.d},dnsmasq{,-shared}.d}
    keepdir /etc/NetworkManager/system-connections
    keepdir /var/lib/NetworkManager
    keepdir /usr/$(exhost --target)/lib/NetworkManager/{conf.d,dispatcher.d{,/pre-{down,up}.d,/no-wait.d},dnsmasq{,-shared}.d,VPN}
    keepdir /usr/$(exhost --target)/lib/NetworkManager/system-connections
}

NetworkManager-meson_pkg_preinst() {
    default

    if option providers:iwd; then
        cat >> "${WORK}"/NetworkManager.conf << EOF
[device]
   wifi.backend = iwd
EOF

        insinto /etc/NetworkManager/
        doins "${WORK}"/NetworkManager.conf
    fi
}

