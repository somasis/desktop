# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=SPIRV-Tools tag=v${PV} ] \
    cmake

SUMMARY="Provides an API and commands for processing SPIR-V modules"
DESCRIPTION="
The project includes an assembler, binary module parser, disassembler, validator, and optimizer for
SPIR-V. Except for the optimizer, all are based on a common static library. The library contains
all of the implementation details, and is used in the standalone tools whilst also enabling
integration into other code bases directly. The optimizer implementation resides in its own
library, which depends on the core library.
"
HOMEPAGE+=" https://www.khronos.org/registry/spir-v"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        sys-libs/spirv-headers[>=1.4.1_p20190830]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DSKIP_SPIRV_TOOLS_INSTALL:BOOL=FALSE
    -DSPIRV_ALLOW_TIMERS:BOOL=TRUE
    -DSPIRV_CHECK_CONTEXT:BOOL=FALSE
    -DSPIRV_COLOR_TERMINAL:BOOL=TRUE
    -DSPIRV-Headers_SOURCE_DIR:PATH=/usr/$(exhost --target)
    -DSPIRV_LOG_DEBUG:BOOL=FALSE
    -DSPIRV_SKIP_EXECUTABLES:BOOL=FALSE
    -DSPIRV_SKIP_TESTS:BOOL=TRUE
    -DSPIRV_TOOLS_INSTALL_EMACS_HELPERS:BOOL=FALSE
    -DSPIRV_WARN_EVERYTHING:BOOL=FALSE
    -DSPIRV_WERROR:BOOL=FALSE
)

