# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PV##*_p}
UBUNTU_RELEASE=16.10

require launchpad
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A library to allow applications to export a menu into the Unity Menu bar"
DOWNLOADS="https://bazaar.launchpad.net/~indicator-applet-developers/${PN}/trunk.${UBUNTU_RELEASE}/tarball/${MY_PNV} -> ${PNV}.tar.gz"

BUGS_TO="keruspe@exherbo.org"

LICENCES="LGPL-2.1 LGPL-3"
SLOT="0.1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk2
    gtk-doc
    mono
"

# Broken, don't build
RESTRICT="test"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools[>=1.14]
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/dbus-glib:1[>=0.82]
        dev-libs/glib:2[>=2.35.4]
        dev-libs/libdbusmenu:0.4[>=0.5.90][gtk2?]
        dev-libs/libindicator:0.4[>=0.4.93][gtk2?]
        x11-libs/gtk+:3[>=2.91][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.7] )
        gtk2? (
            gnome-bindings/pygobject:2[>=0.22]
            gnome-bindings/pygtk:2[>=2.14.0]
            x11-libs/gtk+:2[>=2.18][gobject-introspection?]
        )
        mono? ( gnome-bindings/gtk-sharp:2 )
"

WORK=${WORKBASE}/\~indicator-applet-developers/${PN}/trunk.${UBUNTU_RELEASE}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-mono-test
    --disable-static
    --disable-tests
    --with-gtk=3
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'mono'
)

src_prepare() {
    edo sed -e 's/-Werror//' \
            -i src/Makefile.am \
            -i example/Makefile.am \
            -i tests/Makefile.am

    # Mono can't be disabled with a switch so we require a fantasy version
    if ! option mono ; then
        edo sed -i -e "/MONO_REQUIRED_VERSION/s:1.0:99&:" configure.ac
    fi

    edo gtkdocize --copy
    autotools_src_prepare
}

src_configure() {
    if option gtk2; then
        edo mkdir "${WORKBASE}"/gtk2-build
        edo cp -r "${WORK}"/* "${WORKBASE}"/gtk2-build
        edo pushd "${WORKBASE}"/gtk2-build

        local myconf=(
            "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
            $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_ENABLES[@]}" ; do \
                option_enable ${s} ; \
            done )
        )
        myconf+=( --with-gtk=2 )
        econf "${myconf[@]}"

        edo popd
    fi

    # Avoid pygtk-2.0/pygobject-2.0 dependencies for the gtk3 build
    export APPINDICATOR_PYTHON_CFLAGS=" "
    export APPINDICATOR_PYTHON_LIBS=" "

    default
}

src_compile() {
    if option gtk2; then
        edo pushd "${WORKBASE}"/gtk2-build
        default
        edo popd
    fi

    default
}

src_install() {
    if option gtk2; then
        edo pushd "${WORKBASE}"/gtk2-build
        default
        edo popd
    fi

    default
}

