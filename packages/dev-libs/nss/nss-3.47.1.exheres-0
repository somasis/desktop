# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2012, 2013 Wouter van Kesteren <woutershep@gmail.com>
# Copyright 2012 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'nss-3.12.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

SUMMARY="Mozilla's Network Security Services library that implements PKI support"
DESCRIPTION="
Network Security Services (NSS) is a set of libraries designed to support cross-platform
development of security-enabled client and server applications. Applications built with NSS can
support SSL v3, TLS 1.2, (experimental TLS 1.3), PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME,
X.509 v3 certificates, and other security standards.
"
HOMEPAGE="https://www.mozilla.org/projects/security/pki/${PN}"
DOWNLOADS="https://archive.mozilla.org/pub/mozilla.org/security/${PN}/releases/NSS_${PV//./_}_RTM/src/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="https://developer.mozilla.org/en-US/docs/${PN}/${PN}_${PV}_release_notes"

LICENCES="MPL-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    cacert [[ description = [ Install CAcert's certificates ] ]]
    debug
    utils [[ description = [ Install the included utilities, e. g. certutil for cert management ] ]]
"

DEPENDENCIES="
    build+run:
        app-admin/eclectic[>=2.0.18] [[ note = [ Split ld-*.path, @TARGET@ substitution ] ]]
        dev-db/sqlite:3[>=3.7.15]
        dev-libs/nspr[>=4.12] [[ note = [ Needs PR_GetEnvSecure added in 4.12 ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p2 "${FILES}"/${PN}-3.24-use-PR_htonl.patch
)

WORK=${WORKBASE}/${PNV}/nss

src_prepare() {
    # Respect user LD
    edo sed -e "/^DSO_LDOPTS/s:ld:${LD}:" \
            -i coreconf/Linux.mk

    # The script insists on a fully qualified domain name for localhost and
    # additionally rejects "(none)". "localdomain", which might be an
    # alternative, is not really standardised and we don't have it by
    # default, so we just get rid of the scripts objections.
    edo sed \
        -e '/if \[ "$DOMSUF" = "(none)" \]; then/,+2d' \
        -i tests/common/init.sh

    option cacert && expatch -p2 "${FILES}"/nss-cacert.patch

    default
}

src_configure() {
    # Respect LDFLAGS
    export DSO_LDOPTS="${LDFLAGS}"

    option debug || export BUILD_OPT=1
}

nss_compile() {
    # used by the "buildsystem"
    unset TARGETS

    local myconf=()

    myconf=(
        -j1
        CC="${CC}"
        CCC="${CXX}"
        AR="${AR} rc \$@"
        RANLIB="${RANLIB}"
        NSDISTMODE=copy
        NSPR_INCLUDE_DIR=/usr/$(exhost --target)/include/nspr
        NSS_USE_SYSTEM_SQLITE=1
        OPTIMIZER="${CFLAGS}"
        USE_SYSTEM_ZLIB=1
        ZLIB_LIBS=-lz
        NSS_ENABLE_WERROR=0
        coreconf lib/dbm all
    )

    [[ $(exhost --target) =~ ^(x86_64|aarch64)- ]] && export USE_64=1

    emake "${myconf[@]}"
}

src_compile() {
    nss_compile
}

nss_test() {
    local params=(
        # The suites to run
        NSS_CYCLES="${@}"

        # Bind to loopback
        USE_IP=TRUE
        IP_ADDRESS=127.0.0.1

        # Avoid dep on net-tools for domainname
        DOMSUF='(none)'

        # Fails when "parallel" binary provided by moreutils
        GTESTFILTER="noparallel"
    )

    # temporarily patch stuff to bind to loopback
    edo sed -i -e 's/PR_INADDR_ANY/PR_INADDR_LOOPBACK/g' \
        lib/libpkix/pkix_pl_nss/module/pkix_pl_socket.c \
        cmd/httpserv/httpserv.c \
        cmd/selfserv/selfserv.c \
        cmd/libpkix/pkix_pl/module/test_socket.c
    nss_compile

    edo env "${params[@]}" tests/all.sh

    # revert to the unpatched versions for installing
    edo sed -i -e 's/PR_INADDR_LOOPBACK/PR_INADDR_ANY/g' \
        lib/libpkix/pkix_pl_nss/module/pkix_pl_socket.c \
        cmd/httpserv/httpserv.c \
        cmd/selfserv/selfserv.c \
        cmd/libpkix/pkix_pl/module/test_socket.c
    nss_compile

}

src_test() {
    nss_test standard
}

src_test_expensive() {
    nss_test pkix upgradedb sharedb
}

src_install() {
    edo pushd ../dist
        edo pushd *.OBJ
            export LIBDIR=lib/nss
            dolib lib/*.chk
            dolib.a lib/*.a
            dolib.so lib/*.so
            unset LIBDIR

            option utils && dobin bin/!(*test)
        edo popd

        insinto /usr/$(exhost --target)/include/nss
        doins public/nss/*.h
    edo popd

    edo sed -e "s|@TOOL_PREFIX@|$(exhost --tool-prefix)|" \
            "${FILES}"/nss-config | herebin nss-config

    insinto /usr/$(exhost --target)/lib/pkgconfig
    edo sed -e "s|@EXHOST@|$(exhost --target)|" \
            -e "s|@MAJOR_VERSION@|$(ever major)|" \
            -e "s|@VERSION@|${PV}|" \
            "${FILES}"/nss.pc.in | hereins nss.pc

    hereenvd 40nss <<EOF
LDPATH=/usr/@TARGET@/lib/nss
EOF
}

