# Copyright 2008-2011 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require systemd-service

SUMMARY="Policy framework for controlling privileges for system-wide services"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/releases/${PNV}.tar.gz"

LICENCES="LGPL-2"
SLOT="1"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    ( providers: systemd elogind consolekit ) [[
        *description = [ Login daemon provider ]
        number-selected = exactly-one
    ]]
    ( linguas: cs da de hr hu id pl pt_BR sk sv tr uk zh_CN zh_TW )
"

# Wants to use system dbus socket
RESTRICT="test"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.1.2 [[ description = [ Needed to build man-pages ] ]]
        app-text/docbook-xsl-stylesheets [[ description = [ Needed to build man-pages ] ]]
        dev-libs/libxslt[>=1.1.24]
        dev-util/intltool[>=0.40.0]
        gnome-desktop/gobject-introspection:1 [[ note = [ required for automake ] ]]
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.3] )
    build+run:
        group/polkitd
        user/polkitd
        dev-libs/expat[>=2.0.1]
        dev-libs/glib:2[>=2.30.0]
        sys-libs/pam
        dev-libs/spidermonkey:60
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.2] )
        providers:systemd? ( sys-apps/systemd )
        providers:elogind? ( sys-auth/elogind )
        providers:consolekit? ( sys-auth/ConsoleKit2 )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-configure-fix-elogind-support.patch
    "${FILES}"/${PN}-make-netgroup-support-optional.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-man-pages
    --enable-nls
    --disable-examples
    --disable-static
    --localstatedir=/var
    --with-authfw=pam
    --with-os-type=gentoo
    --with-polkitd-user=polkitd
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    gtk-doc
    'providers:elogind libelogind'
    'providers:systemd libsystemd-login'
)

DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-test --disable-test'
)

src_prepare() {
    autotools_src_prepare

    # fix shebang
    edo sed -e 's:^#!.*:#!/usr/bin/env perl:' \
            -i src/polkitbackend/toarray.pl
}

src_install() {
    default

    keepdir /usr/share/polkit-1/rules.d

    edo chown -R polkitd:root "${IMAGE}"{etc,usr/share}/polkit-1/rules.d
    edo chmod 0700 "${IMAGE}"/{etc,usr/share}/polkit-1/rules.d
}

