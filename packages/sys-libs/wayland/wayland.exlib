# Copyright 2012 Arne Janbu
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.13 1.12 ] ]

export_exlib_phases src_configure src_test

SUMMARY="The Wayland display protocol library (client and server)"
HOMEPAGE="https://wayland.freedesktop.org/"

SLOT="0"
LICENCES="MIT"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        dev-libs/libxml2:2.0
        doc?
        (
            app-doc/doxygen[>=1.6.0]
            app-text/xmlto
            media-gfx/graphviz[>=2.26.0]
        )
        virtual/pkg-config
    build+run:
        dev-libs/expat
        dev-libs/libffi
        !x11-dri/mesa[<18.0.3] [[
            description = [ wayland imported libwayland-egl from mesa ]
            resolution = uninstall-blocked-after
        ]]
"

BUGS_TO="sardemff7@exherbo.org devel@arnej.de"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/wayland-1.7.0-disable-ptrace-dependent-tests.patch
)

DEFAULT_SRC_TEST_PARAMS=( -j1 )

wayland_src_configure() {
    local myconf=()

    myconf+=(
        --enable-libraries
        $(option_enable doc documentation)
    )

    if ! exhost --is-native -q ; then
        myconf+=( --with-host-scanner )
    fi

    econf "${myconf[@]}"
}

wayland_src_test() {
    esandbox allow_net unix:/tmp/wayland-tests-*/wayland-*

    # Or else the path is too long for the test buffer
    XDG_RUNTIME_DIR=/tmp default

    esandbox disallow_net unix:/tmp/wayland-tests-*/wayland-*
}

